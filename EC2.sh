#!/bin/bash

fecha=$(date +%F_%H_%M)

aws --region us-east-1 ec2 describe-instances --output text --query "Reservations[].Instances[].[[Tags[?Key=='Name'].Value] [0][0],';', InstanceId,';', PrivateIpAddress,';', to_string([SecurityGroups[].GroupName]),';', VpcId ,';', SubnetId,';', 'Virginia-us-east-1',';', 'PRODUCCION',';', [Tags[?Key=='Descripcion'].Value] [0][0], ';', ImageId , ';', State.Name ]" | sed 's/[^A-Za-z0-9.áéíóú;,_ -]//g' | sed 's/None/NO ESPECIFICADO/g' | sort -f >> AWS_DAT/EC2_AWS_$fecha.csv
aws --region us-west-2 ec2 describe-instances --output text --query "Reservations[].Instances[].[[Tags[?Key=='Name'].Value] [0][0],';', InstanceId,';', PrivateIpAddress,';', to_string([SecurityGroups[].GroupName]),';', VpcId ,';', SubnetId,';', 'Oregon-us-west-2',';', 'PRODUCCION',';', [Tags[?Key=='Descripcion'].Value] [0][0], ';', ImageId , ';', State.Name ]" | sed 's/[^A-Za-z0-9.áéíóú;,_ -]//g' | sed 's/None/NO ESPECIFICADO/g' | sort -f >> AWS_DAT/EC2_AWS_$fecha.csv

cat AWS_DAT/EC2_AWS_$fecha.csv | sed 's/;/:/g' | sed 's/ /*/g' >> AWS_DAT/EC2_PRC_$fecha.csv
 
echo "NAME_EC2;INSTANCE_ID;IP_ADDRESS;SECURITY_GROUP_NAME;VPC_ID;SUBNET_ID;REGION;Es PCI? ;AMBIENTE;DESCRIPCION;AMI;STATE" >> EC2_$fecha.csv
for i in $(awk -F: '{print $1":"$2":"$3":"$4":"$5":"$6":"$7":"$8":"$9":"$10":"$11}' AWS_DAT/EC2_PRC_$fecha.csv);do
          col1=$(echo $i | cut -d ":" -f 1)
	      col2=$(echo $i | cut -d ":" -f 2)
          col3=$(echo $i | cut -d ":" -f 3)
          col4=$(echo $i | cut -d ":" -f 4)
          col5=$(echo $i | cut -d ":" -f 5)
          col6=$(echo $i | cut -d ":" -f 6)
          col7=$(echo $i | cut -d ":" -f 7)
		  col8=$(echo $i | cut -d ":" -f 8)
		  col9=$(echo $i | cut -d ":" -f 9)
		  col11=$(echo $i | cut -d ":" -f 10)
		  col12=$(echo $i | cut -d ":" -f 11)


          col13=$(aws ec2 describe-images --image-ids $col11 --output text --query "Images[].[Architecture,';', PlatformDetails,';' , Description]")


		 if grep -Fxq "$col2" Files/ListPCI.dat ; then  col10="SI"; else  col10="NO" ; fi
		  
		 echo $col1";"$col2";"$col3";"$col4";"$col5";"$col6";"$col7";"$col10";"$col8";"$col9";"$col11";"$col12";"$col13  | sed 's/*/ /g' | sed 'y/áéíóú/aeiou/' >> EC2_$fecha.csv
		 
 done



